<?php

require_once "terminal-lib.php";

/**
 * Draw progress bar
 * Limit length to 50 to keep within terminal window
 * @param  int          $done     Current step completed
 * @param  int          $total    Max steps
 * @param  bool|boolean $colorize true to use colors or false to use system colors
 */
function progressBar(int $done, int $total, bool $colorize = true) {
    $MAX_LENGTH = 50;
    $norm = (int)(normalize($done, 0, $total) * $MAX_LENGTH);
    $perc = (int)(($done / $total) * 100);
    if ($colorize) {
        $bar = CLI_BLACK . CLI_BG_GREEN . "Progress {$perc}%" . CLI_RESET . CLI_YELLOW . " [" . ($perc > 0 ? str_repeat("#", $norm) : '') . "]" . CLI_RESET;
    } else {
        $bar = "{$perc}% [" . ($perc > 0 ? str_repeat("#", $norm) : '') . "]";
    }
    Terminal\clearCliLine(100);
    fwrite(STDOUT, $bar);
}

/**
 * Normalize value to 0-1
 * @param  int      $value The value to normalize
 * @param  int      $min   The old minimum
 * @param  int      $max   The old maximum
 * @return float           The normalized value between 0 & 1
 */
function normalize(float $value, int $min, int $max): float {
    return (($value - $min) / ($max - $min));
}
