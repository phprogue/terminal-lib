<?php

/*
 * SAMPLE USAGE OF terminal-lib.php
 * AND progress-bar.php
 */

require_once "terminal-lib.php";
include "progress-bar.php";

function snooze(?int $seconds = 1)
{
    sleep($seconds);
}

Terminal\clearScreen();

Terminal\memoryUsage("start");
Terminal\benchmark("start");

Terminal\output("first line of text", "green");
snooze();
Terminal\moveCursorBack(5);
Terminal\output("move back 5", "white", "red");
snooze();
Terminal\moveCursorTo(10, 5);
Terminal\output("I'm at 10,5", "asdf"); // will ignore an unidentified color
snooze();
Terminal\saveCursorPos();
Terminal\moveCursorTo(5, 10);
Terminal\output("I'm at 5,10", null, "yellow");
snooze();
Terminal\restoreCursorPos();
//echo CLI_BOLD;
Terminal\output("Now I'm restored", "black", "white");
snooze();
Terminal\moveCursorTo(0, 14);

// for ($i=16; $i < 256; $i++) {
//     echo "\033[48;5;{$i}m" . $i . " ";
// }
Terminal\resetStyle();

// test file logging
Terminal\fileLog("This is a logging test");
Terminal\fileLog();

echo "\n";

sleep(1);
Terminal\benchmark("stop");

// create a array of data for memory test
$memArray = [];
for ($d = 0; $d < 10000; $d++) {
    $memArray[] = $d * 100;
}
Terminal\memoryUsage("stop", "kb");
$memArray = [];

echo "\n\n";

/* PROGRESS BAR USAGE */
Terminal\hideCursor();
for ($i = 0; $i <= 100; $i++) {
    progressBar($i, 100, true);
    for ($x = 0; $x < 8000000; $x++) {
        // nothing
    }
}
//Terminal\bell();
Terminal\showCursor();

echo "\n";
