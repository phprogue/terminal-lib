<?php

namespace Terminal;
// sample usage: Terminal\clearScreen();

/* Set colors for cli */

define("CLI_BLACK", "\033[30m");
define("CLI_GREEN", "\033[32m");
define("CLI_RED", "\033[31m");
define("CLI_YELLOW", "\033[33m");
define("CLI_BLUE", "\033[34m");
define("CLI_WHITE", "\033[37m");
define("CLI_BG_RED", "\033[41m");
define("CLI_BG_GREEN", "\033[42m");
define("CLI_BG_YELLOW", "\033[43m");
define("CLI_BG_WHITE", "\033[47m");
define("CLI_BOLD", "\033[1m");
define("CLI_RESET", "\033[0m");

/**
 * Clear current line and reset cursor to beginning of line
 * @param  int|integer $cols The number of columns to move cursor backwards
 */
function clearCliLine(int $cols = 80)
{
    // move cursor back n cols (beginning)
    echo "\033[{$cols}D";
    // erase line
    echo "\033[K";
}

/**
 * Clear screen and reset cursor to 0,0
 */
function clearScreen()
{
    echo "\033[2J";
    echo "\033[H";
}

function moveCursorTo(int $x = 0, int $y = 0)
{
    echo "\033[{$y};{$x}H";
}

function moveCursorBack(int $x = 80)
{
    echo "\033[{$x}D";
}

function saveCursorPos()
{
    echo "\033[s";
}

function restoreCursorPos()
{
    echo "\033[u";
}

function hideCursor()
{
    echo "\033[?25l";
}

function showCursor()
{
    echo "\033[?25h";
}

/**
 * Reset test styling to default
 * This resets color, background color, and all other attributes
 */
function resetStyle()
{
    echo CLI_RESET;
}

/**
 * does not work without a motherboard speaker
 */
function bell()
{
    fwrite(STDOUT, "\x07");
}

/**
 * Send text to STDOUT with coloring
 * Resets text styling before returning
 * @param  string      $text    The test to display
 * @param  string|null $fgColor Foreground color: yellow, green, red, etc
 * @param  string|null $bgColor Background color: yellow, green, red, etc
 */
function output(string $text, string $fgColor = null, string $bgColor = null)
{
    if ($bgColor != null) {
        // suppress warnings for unidentified constants
        try {
            if (constant("CLI_BG_" . strtoupper($bgColor))) {
                $bgColor = constant("CLI_BG_" . strtoupper($bgColor));
            }
        } catch (\Throwable $t) {
            $bgColor = null;
        }
    }
    if ($fgColor != null) {
        try {
            if (constant("CLI_" . strtoupper($fgColor))) {
                $fgColor = constant("CLI_" . strtoupper($fgColor));
            }
        } catch (\Throwable $t) {
            $fgColor = null;
        }
    }

    fwrite(STDOUT, $bgColor . $fgColor . $text);
    resetStyle();
}

/**
 * Simple file logger
 * Creates one file per day and uses date as file name
 *
 * @param  string $msg The message to write
 *                     Appends a newline if not included with $msg
 */
function fileLog(string $msg = "")
{
    $filename = date("Ymd") . ".log";

    $dt = new \DateTime;
    $timestamp = $dt->format("Y-m-d H:i:s.v") . " ";

    if (strpos($msg, "\n") != strlen($msg) - 1) {
        $msg .= "\n";
    }

    $fp = fopen($filename, "a");
    fwrite($fp, $timestamp . $msg);
    fclose($fp);
}

/**
 * Benchmark execution time of arbitrary code block
 * @param  string $action "start" or "stop"
 *                        Passing "stop" as argument will calculate and output time elapsed
 */
function benchmark(string $action = "start")
{
    static $startTime = 0;
    static $endTime = 0;

    if ($action == "start") {
        $startTime = microtime(true);
    } else if ($action == "stop") {
        $endTime = microtime(true);
    } else {
        return;
    }

    if ($endTime > 0) {
        printf("Benchmark time: %0.2f seconds\n", $endTime - $startTime);
        $endTime = 0;
    }
}

/**
 * Show memory usage of arbitrary section of script
 * @param  string      $action  "start" or "stop"
 *                              Passing "stop" as argument will calculate and output memory usage
 * @param  string|null $newSize "MB" or "KB" to denote which size to display
 */
function memoryUsage(string $action = "start", string $newSize = null)
{
    static $startMem = 0;
    static $endMem = 0;
    $size = "MB";
    $sizeNum = 1000000;

    if ($action == "start") {
        $startMem = memory_get_usage();
    } else if ($action == "stop") {
        $endMem = memory_get_usage();
    } else {
        return;
    }

    if (strtoupper($newSize) == "KB") {
        $size = "KB";
        $sizeNum = 1000;
    }

    if ($endMem > 0) {
        printf("Memory usage: %0.2f %s\n", ($endMem - $startMem) / $sizeNum, $size);
        $endMem = 0;
    }
}

/**
 * Determine if request is from CLI or Web
 * @return boolean true if request is from CLI or false if not
 */
function isCli()
{
    if (defined('STDIN')) {
        return true;
    }

    if (php_sapi_name() === 'cli') {
        return true;
    }

    if (array_key_exists('SHELL', $_ENV)) {
        return true;
    }

    if (!array_key_exists('REQUEST_METHOD', $_SERVER)) {
        return true;
    }

    return false;
}

