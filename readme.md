# Terminal Library

Create simple functions to aid in writing command line scripts in PHP

1. define colors
2. methods to position cursor using ANSI escape sequences
3. clear screen
4. progress bar
    a. Draw 100% bar width, then fill with '#' as it progresses
5. more to come...

